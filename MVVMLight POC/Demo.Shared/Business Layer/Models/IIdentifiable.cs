﻿namespace Demo.Shared.Models
{
    public interface IIdentifiable
    {
        int Id { get; set; }
    }
}