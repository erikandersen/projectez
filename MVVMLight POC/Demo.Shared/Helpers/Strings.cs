﻿namespace Demo.Shared.Helpers
{
    public static class Strings
    {
        public static class AppConstants
        {
            public static string SoccerSeasons = "European Soccer Seasons";
            public static string Teams = "Teams";
            public static string Ok = "OK";
            public static string TryAgainLater = "Please Try Again Later";
            public static string EnterEmail = "Please Enter a Valid Email Address";
            public static string AppTitle = "Xamarin Demo";
        }

        public static class Settings
        {
            public static string XamarinInsightsApiKey = "af1e96ed88afa4ff4c07612e65edc73bf9d0a1d6";
        }
    }
}