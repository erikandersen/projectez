﻿namespace Demo.Shared.Helpers
{
    public static class Colors
    {
        public static string ThemePrimary = "#5dc19c";
        public static string ThemeSecondary = "#FFFFFF";
        public static string ThemeTertiary = "#333333";
    }
}