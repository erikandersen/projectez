// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using System;
using MonoTouch.Foundation;
using MonoTouch.UIKit;
using System.CodeDom.Compiler;

namespace Demo.iOS.Controllers
{
	[Register ("SeasonViewController")]
	partial class SeasonViewController
	{
		[Outlet]
		MonoTouch.UIKit.UITableView TableView { get; set; }

		void ReleaseDesignerOutlets ()
		{
		}
	}
}
